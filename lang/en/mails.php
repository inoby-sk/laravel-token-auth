<?php 

return [
  "password_reset_code" => [
    "heading" => "Verification code to reset your password",
    "code_text" => "Enter the following code in the app when prompted:",
    "code_expiry" => "The code is valid for :minutes minutes from the time the email was sent."
  ]
];