# Laravel authorization for mobile apps (Laravel sanctum)

Defines common routes and controllers for authorization. Uses laravel sanctum for token handling.

## Features

- Simple string-based roles for users
- Routes for user management
- Configurable roles for user management (e.g. only admin can create users)
- Password reset

## Installation

1. Add dependency to your laravel project

```bash
composer require inoby-sk/laravel-token-auth
```

2. Publishing provider and actions

```bash
php artisan vendor:publish --provider="Inoby\Laravel\TokenAuth\TokenAuthServiceProvider"
```

3. Add traits to your user model

```diff
<?php

  namespace App\Models;

  use Illuminate\Foundation\Auth\User as Authenticable;
+ use Inoby\Laravel\TokenAuth\Traits\HasRoles;
+ use Inoby\Laravel\TokenAuth\Traits\HasPermissions;
  use Laravel\Sanctum\HasApiTokens;

  class User extends Authenticable {
-   use HasApiTokens;
+   use HasApiTokens, HasRoles, HasPermissions;

    ...
  }
```

4. Register provider

```php
// config/app.php
"providers" => [
  ...
  App\Providers\TokenAuthServiceProvider::class
]
```

5. Run migrations

```bash
php artisan migrate
```

## Configuration

TODO: Add config description
