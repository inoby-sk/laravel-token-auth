@component('mail::message')
# {{ __("laravel-token-auth::mails.password_reset_code.heading") }}

{{ __("laravel-token-auth::mails.password_reset_code.code_text") }}

@component('mail::panel')
`{{ $code }}`
@endcomponent

{{ __("laravel-token-auth::mails.password_reset_code.code_expiry", ["minutes" => 15]) }}
@endcomponent