<?php

namespace Inoby\Laravel\TokenAuth\Tests;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Config;
use Inoby\Laravel\TokenAuth\Contracts\CreatesNewUsers;
use Inoby\Laravel\TokenAuth\TokenAuth;
use Mockery;

class PublicRegisterTest extends TestCase {

  public function test_that_new_users_can_be_registered()
  {
    $this->mock(CreatesNewUsers::class)
      ->shouldReceive('create')
      ->andReturn(Mockery::mock(Authenticatable::class));

    $response = $this->postJson($this->getApiRoute("/register"), []);

    $response->assertStatus(201)->assertJson(["success" => true]);
  }
}