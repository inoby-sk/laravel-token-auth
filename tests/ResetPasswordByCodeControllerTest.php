<?php

namespace Inoby\Laravel\TokenAuth\Tests;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Inoby\Laravel\TokenAuth\Contracts\ResetsPassword;
use Inoby\Laravel\TokenAuth\Mail\PasswordResetCodeMail;
use Inoby\Laravel\TokenAuth\Models\PasswordResetCode;
use Mockery;

class ResetPasswordByCodeControllerTest extends TestCase {

  public function test_that_user_can_opt_in_reset_code()
  {
    Mail::fake();

    $user = $this->createUser(["email" => "test@example.com", "password" => bcrypt("secret")]);

    $this->postJson($this->getApiRoute("/password/email"), [
      "email" => "test@example.com",
    ]);

    Mail::assertSent(PasswordResetCodeMail::class, function (PasswordResetCodeMail $mail) use ($user) {
      return $mail->hasTo($user->email, $user->name);
    });
  }

  public function test_that_mail_reset_code_is_valid()
  {

    $user = $this->createUser(["email" => "test@example.com"]);

    $resetCode = PasswordResetCode::create([
      "email" =>  $user->email,
      "code" => "123456"
    ]);

    $response = $this->postJson($this->getApiRoute("/password/check-code"), [
      "email" => "test@example.com",
      "code" => "123456"
    ]);

    $response->assertStatus(200);
  }

  public function test_that_mail_reset_code_is_invalid()
  {
    $user = $this->createUser(["email" => "test@example.com"]);

    $resetCode = PasswordResetCode::create([
      "email" =>  $user->email,
      "code" => "123456"
    ]);

    $response = $this->postJson($this->getApiRoute("/password/check-code"), [
      "email" => $user->email,
      "code" => "bad-code"
    ]);

    $response->assertStatus(422);
  }

  public function test_that_mail_reset_code_is_invalid_for_diferent_user()
  {
    $user = $this->createUser(["email" => "test@example.com"]);

    $resetCode = PasswordResetCode::create([
      "email" =>  $user->email,
      "code" => "123456"
    ]);

    $response = $this->postJson($this->getApiRoute("/password/check-code"), [
      "email" => "different@example.com",
      "code" => "123456"
    ]);

    $response->assertStatus(422);
  }

  public function test_user_can_reset_password()
  {

    Password::shouldReceive('broker')->andReturn($broker = Mockery::mock(PasswordBroker::class));
    
    $user = $this->mock(Authenticatable::class);
    $user->shouldReceive('save')->once();

    $this->mock(ResetsPassword::class)
      ->shouldReceive('reset')
      ->once()
      ->with($user, Mockery::type('array'));

    $broker->shouldReceive("reset")->andReturnUsing(function ($input, $callback) use ($user) {
      $callback($user, 'new-secret');
      return Password::PASSWORD_RESET;
    });

    $response = $this->postJson($this->getApiRoute("/password/reset"), [
      "email" => "test@example.com",
      "token" => "token",
      "password" => "new-secret",
      "password_confirmation" => "new-secret"
    ]);

    $response->assertStatus(200);
  }

  public function test_user_cannot_reset_password_with_invalid_token()
  {
    Password::shouldReceive('broker')->andReturn($broker = Mockery::mock(PasswordBroker::class));

    $broker->shouldReceive('reset')->andReturnUsing(function ($input, $callback) {
        return Password::INVALID_TOKEN;
    });

    $response = $this->postJson($this->getApiRoute("/password/reset"), [
        'token' => 'token',
        'email' => 'test@example.com',
        'password' => 'password',
        'password_confirmation' => 'password',
    ]);

    $response->assertStatus(422);
    $response->assertJsonValidationErrors('email');
  }
}