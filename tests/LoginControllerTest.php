<?php

namespace Inoby\Laravel\TokenAuth\Tests;

class LoginControllerTest extends TestCase
{
    public function test_that_user_can_login()
    {
        $this->createUser(["email" => "test@example.com", "password" => bcrypt("secret")]);

        $response = $this->postJson($this->getApiRoute("/login"), [
            "email" => "test@example.com",
            "password" => "secret",
            "device_name" => "device",
        ]);

        $response->assertStatus(200);
    }

    public function test_that_user_cannot_login_with_bad_credentials()
    {
        $this->createUser(["email" => "test@example.com", "password" => bcrypt("secret")]);

        $response = $this->postJson($this->getApiRoute("/login"), [
            "email" => "test@example.com",
            "password" => "bad_secret",
            "device_name" => "device",
        ]);

        $response->assertInvalid(["password"]);
    }
}
