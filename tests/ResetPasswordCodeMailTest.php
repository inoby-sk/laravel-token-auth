<?php

namespace Inoby\Laravel\TokenAuth\Tests;

use Inoby\Laravel\TokenAuth\Mail\PasswordResetCodeMail;
use Inoby\Laravel\TokenAuth\Models\PasswordResetCode;

class ResetPasswordCodeMailTest extends TestCase {

  public function test_that_mailable_contains_reset_code()
  {

    $user = $this->createUser(["email" => "test@example.com"]);
    $resetCode = PasswordResetCode::make([
      "email" =>  $user->email,
      "code" => "123456"
    ]);


    $mailable = new PasswordResetCodeMail($resetCode, $user);

    $mailable->assertSeeInHtml("123456");
  }
}