<?php

namespace Inoby\Laravel\TokenAuth\Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase as Orchestra;
use Inoby\Laravel\TokenAuth\TokenAuthServiceProvider;
use Laravel\Sanctum\SanctumServiceProvider;
use Mockery;

abstract class TestCase extends Orchestra
{
    use RefreshDatabase;

    protected static $config;

    protected function setUp(): void
    {
        parent::setUp();

        self::$config = config("laravel-token-auth");
    }

    protected function defineDatabaseMigrations()
    {
        $this->loadLaravelMigrations();
    }

    protected function getPackageProviders($app)
    {
        return [
            TokenAuthServiceProvider::class,
            SanctumServiceProvider::class,
        ];
    }

    protected function getApiRoute(string $endpoint) {
      $prefix = self::$config["prefix"];
      $endpoint = trim($endpoint, "/");

      return $prefix ? "$prefix/$endpoint" : "/$endpoint"; 
    }


    protected function getTestRoute() {
      return "/laravel-token-auth/test";
    }

    protected function createUser($attrs = []) {
      return User::forceCreate(array_merge([
        "name" => "User",
        "email" => "email@example.com",
        "password" => bcrypt("secret"),
        "role" => null,
      ], $attrs));
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }
}