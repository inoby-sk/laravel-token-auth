<?php

namespace Inoby\Laravel\TokenAuth\Tests;

use Illuminate\Contracts\Auth\Authenticatable;
use Inoby\Laravel\TokenAuth\Contracts\CreatesNewUsers;
use Inoby\Laravel\TokenAuth\TokenAuth;
use Mockery;

class RegisterControllerTest extends TestCase {

  public function test_that_unauthorized_users_cannot_create_users()
  {
    $this->mock(CreatesNewUsers::class)
      ->shouldReceive('create')
      ->andReturn(Mockery::mock(Authenticatable::class));

    $this
      ->mock(TokenAuth::class)
      ->shouldReceive("publiclyRegisterable")
      ->andReturn(false);
    
    $response = $this->postJson($this->getApiRoute("/register"), [
      "name" => "Name Surname",
      "email" => "email@example.com",
      "role" => "admin",
      "password" => "password",
      "password_confirmation" => "password",
    ]);

    $response->assertUnauthorized();
  }

  public function test_that_only_certain_roles_can_create_users()
  {
    $this->mock(CreatesNewUsers::class)
      ->shouldReceive('create')
      ->andReturn(Mockery::mock(Authenticatable::class));

    $admin_user = $this->createUser(["role" => "admin", "email" => "admin@example.com"]);
    $basic_user = $this->createUser(["role" => null, "email" => "basic@example.com"]);

    $this->actingAs($admin_user);
    
    $response = $this->postJson($this->getApiRoute("/register"), [
      "name" => "New User",
      "email" => "new@example.com",
      "password" => "password",
      "password_confirmation" => "password",
    ]);

    $response->assertStatus(201);

    $this->actingAs($basic_user);

    $response = $this->postJson($this->getApiRoute("/register"), [
      "name" => "New User",
      "email" => "new2@example.com",
      "password" => "password",
      "password_confirmation" => "password",
    ]);

    $response->assertForbidden();
  }

  public function getEnvironmentSetUp($app)
  {
    $app['config']->set("laravel-token-auth.public_register", false);
  }
}