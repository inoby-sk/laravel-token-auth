<?php

namespace Inoby\Laravel\TokenAuth\Tests;

use Illuminate\Support\Facades\Hash;
use Inoby\Laravel\TokenAuth\Contracts\UpdatesUserProfileInformation;

class UserControllerTest extends TestCase {
  public function test_logged_in_user_can_access_me_endpoint()
  {
    $user = $this->createUser(["email" => "test@example.com", "password" => bcrypt("secret")]);
    $this->actingAs($user);
    
    $response = $this->getJson($this->getApiRoute("/me"));

    $response->assertJsonStructure(["user"])->assertOk();
  }

  public function test_not_logged_in_user_cannot_access_me_endpoint()
  {
    $response = $this->getJson($this->getApiRoute("/me"));

    $response->assertUnauthorized();
  }

  public function test_user_can_update_own_profile_information()
  {
    $this->mock(UpdatesUserProfileInformation::class)
      ->shouldReceive('update')
      ->andReturn(true);

    $user = $this->createUser();

    $this->actingAs($user);


    $response = $this->putJson($this->getApiRoute("/users/$user->id"), []);

    $response->assertOk();
  }

  public function test_user_cannot_update_other_profile_information()
  {
    $this->mock(UpdatesUserProfileInformation::class)
      ->shouldReceive('update')
      ->andReturn(true);

    $user = $this->createUser(["role" => null]);
    $other = $this->createUser(["role" => "admin", "email" => "diff@email.com"]);

    $this->actingAs($user);

    $response = $this->putJson($this->getApiRoute("/users/$other->id"), []);

    $response->assertForbidden();
  }

  public function test_admin_user_can_update_other_profile_information()
  {
    $this->mock(UpdatesUserProfileInformation::class)
      ->shouldReceive('update')
      ->andReturn(true);

    $admin = $this->createUser(["role" => "admin", "email" => "admin@email.com"]);
    $user = $this->createUser(["role" => null]);

    $this->actingAs($admin);

    $response = $this->putJson($this->getApiRoute("/users/$user->id"), []);

    $response->assertOk();
  }

  public function test_user_can_change_password() {
    $user = $this->createUser(["password" => bcrypt("secret")]);

    $this->actingAs($user);

    $response = $this->postJson($this->getApiRoute("/password/change"), [
      "old_password" => "secret",
      "password" => "new_password",
      "password_confirmation" => "new_password"
    ]);

    $response->assertOk();
  }

  public function test_user_cannot_change_password_without_old_password() {
    $user = $this->createUser(["password" => bcrypt("secret")]);

    $this->actingAs($user);

    $response = $this->postJson($this->getApiRoute("/password/change"), [
      "password" => "new_password",
      "password_confirmation" => "new_password"
    ]);

    $response->assertUnprocessable()->assertInvalid(["old_password"]);
  }

  public function test_user_cannot_change_password_with_wrong_old_password() {
    $user = $this->createUser(["password" => bcrypt("secret")]);

    $this->actingAs($user);

    $response = $this->postJson($this->getApiRoute("/password/change"), [
      "old_password" => "bad_secret",
      "password" => "new_password",
      "password_confirmation" => "new_password"
    ]);

    $response->assertUnprocessable()->assertInvalid(["old_password"]);
  }
}