<?php

namespace Inoby\Laravel\TokenAuth\Tests;

class LogoutControllerTest extends TestCase {

  public function test_that_logged_in_user_can_logout()
  {
    $this->createUser(["email" => "test@example.com", "password" => bcrypt("secret")]);
    
    $response = $this->postJson($this->getApiRoute("/login"), [
      "email" => "test@example.com",
      "password" => "secret",
      "device_name" => "device"
    ]);

    $response->assertJsonStructure(["token", "user"]);

    $token = $response->decodeResponseJson()["token"];

    $response = $this->postJson($this->getApiRoute("/logout"), [], [
      "Authorization" => "Bearer $token"
    ]);

    $response->assertOk();
  }

}