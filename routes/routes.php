<?php

use Inoby\Laravel\TokenAuth\Http\Controllers\LoginController;
use Inoby\Laravel\TokenAuth\Http\Controllers\LogoutController;
use Inoby\Laravel\TokenAuth\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;
use Inoby\Laravel\TokenAuth\Http\Controllers\ChangePasswordController;
use Inoby\Laravel\TokenAuth\Http\Controllers\ResetPasswordByCodeController;
use Inoby\Laravel\TokenAuth\Http\Controllers\ResetPasswordByLinkController;
use Inoby\Laravel\TokenAuth\Http\Controllers\UserController;
use Inoby\Laravel\TokenAuth\TokenAuth;

Route::group(["middleware" => "guest"], function () {
    if (TokenAuth::featureEnabled("reset_password")) {
        if (TokenAuth::resetPasswordUsingCode()) {
            Route::post("/password/email", [ResetPasswordByCodeController::class, "sendResetCode"])
                ->middleware("throttle:5,1")
                ->name("password.email");
            Route::post("/password/check-code", [ResetPasswordByCodeController::class, "checkResetCode"])->name("password.check_code");
            Route::post("/password/reset", [ResetPasswordByCodeController::class, "reset"])->name("password.reset");
        } else {
            Route::post("/password/email", [ResetPasswordByLinkController::class, "sendResetLink"])
                ->middleware("throttle:5,1")
                ->name("password.email");
            Route::post("/password/reset", [ResetPasswordByLinkController::class, "reset"])->name("password.reset");
        }
    }
});

Route::group(["middleware" => "auth:sanctum"], function () {
    if (TokenAuth::featureEnabled("logout")) {
        Route::post("/logout", LogoutController::class)->name("logout");
    }

    if (TokenAuth::featureEnabled("crud")) {
        Route::get("/me", [UserController::class, "me"])->name("me");
        Route::get("/users", [UserController::class, "index"])->name("users.index");
        Route::get("/users/{id}", [UserController::class, "show"])->name("users.show");
        Route::put("/users/{id}", [UserController::class, "update"])->name("users.update");
        Route::delete("/users/{id}", [UserController::class, "delete"])->name("users.delete");
    }

    if (TokenAuth::featureEnabled("change_password")) {
        Route::post("/password/change", [ChangePasswordController::class, "changePassword"])->name("password.change");
    }
});

if (TokenAuth::featureEnabled("login")) {
    Route::post("/login", LoginController::class)
        ->middleware("guest")
        ->name("login");
}

if (TokenAuth::featureEnabled("register")) {
    Route::post("/register", RegisterController::class)
        ->middleware(TokenAuth::publiclyRegisterable() ? "guest" : "auth:sanctum")
        ->name("register");
}
