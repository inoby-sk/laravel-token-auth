<?php

namespace App\Actions\TokenAuth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Inoby\Laravel\TokenAuth\Contracts\UpdatesUserProfileInformation;
use Inoby\Laravel\TokenAuth\TokenAuth;

class UpdateProfileInformation implements UpdatesUserProfileInformation
{

    /**
     * Update user profile information
     *
     * @param \App\Models\User $user
     * @param array $input
     * @return void
     */
    public function update($user, $input) {
      $rules =  [
        "name" => ["required", "string", "max:255"],
        "email" => ["required", "email", Rule::unique('users')->ignore($user->id)],
      ];

      if ($roles = TokenAuth::roles()) {
        $rules["role"] = ["nullable", "in:" . implode(",", $roles)];
      }

      Validator::make($input, $rules)->validate();

      $update_data = [
        'name' => $input['name'],
        'email' => $input['email'],
      ];


      if (isset($input["role"])) {
        $update_data['role'] = $input['role'];

        if (($input['role'] !== $user->role) && !request()->user()->hasPermissionTo("users.change_roles")) {
          throw ValidationException::withMessages(["role" => __("Change of role is forbidden")])->status(403);
        }
      }
      
      return $user->forceFill($update_data)->save();
    }
}