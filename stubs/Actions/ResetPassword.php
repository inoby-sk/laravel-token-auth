<?php

namespace App\Actions\TokenAuth;

use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Inoby\Laravel\TokenAuth\Contracts\ResetsPassword;

class ResetPassword implements ResetsPassword
{
  public function reset(User $user, array $input) {

    Validator::make($input, [
        'password' => "required|min:8|confirmed",
    ])->validate();

    $user->forceFill([
        'password' => Hash::make($input['password']),
    ])->save();
  }
}