<?php

namespace App\Actions\TokenAuth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Inoby\Laravel\TokenAuth\Contracts\CreatesNewUsers;
use Inoby\Laravel\TokenAuth\TokenAuth;

class CreateNewUser implements CreatesNewUsers
{

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, string>  $input
     */
    public function create(array $input): User
    {

      $rules =  [
        "name" => ["required", "string", "max:255"],
        "email" => ["required", "email", "unique:users", "email"],
        "password" => ["required", "min:6", "confirmed"],
      ];

      if ($roles = TokenAuth::roles()) {
        $rules["role"] = ["nullable", "in:" . implode(",", $roles)];
      }

      Validator::make($input, $rules)->validate();

      return User::create([
          'name' => $input['name'],
          'email' => $input['email'],
          'role' => $input['role'] ?? null,
          'password' => Hash::make($input['password']),
      ]);
    }
}