<?php

namespace App\Providers;

use App\Actions\TokenAuth\CreateNewUser;
use App\Actions\TokenAuth\ResetPassword;
use App\Actions\TokenAuth\UpdateProfileInformation;
use Illuminate\Support\ServiceProvider;
use Inoby\Laravel\TokenAuth\TokenAuth;

class TokenAuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
      //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
      TokenAuth::createUsersUsing(CreateNewUser::class);
      TokenAuth::updateUserProfileInformationUsing(UpdateProfileInformation::class);
      TokenAuth::resetUserPasswordUsing(ResetPassword::class);
    }
}
