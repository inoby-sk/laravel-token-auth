<?php

return [
  /**
   * Middleware for auth routes
   */
  "middleware" => "api",

  /**
   * Prefix for auth routes
   */
  "prefix" => "api",

  /**
   * User model
   */
  "model" => App\Models\User::class,

  /**
   * Available roles - can be nullable
   */
  "roles" => ["admin", "subscriber"],

  /**
   * Whether or not to expose '/register' route publicly
   * If set to true, only users with permission to 'users.create'
   * will be authorized to register new users (with default policy).
   */
  "public_register" => true,

  /**
   * Map with permissions to each role where key of the group defines "scope" (model)
   * which permission applies to and values are the permissions.
   *
   * You can define wildcard for all permissions as follows:
   *
   * "role" => [
   *      "model" => "*"
   *  ]
   */
  "permissions" => [
    "admin" => [
      "users" => "*",
    ],
    "subscriber" => [
      "users" => [
        "view_any",
        // "create",
        // "update",
        // "delete",
        // "change_roles"
      ],
    ],
  ],

  /**
   * Reset password strategy
   * Possible values - code | link
   */
  "reset_password_using" => "code",

  /**
   * RoutesPfeatures that should be enabled
   */
  "features" => [
    "login" => true,
    "logout" => true,
    "register" => true,
    "change_password" => true,
    "reset_password" => true,
    "crud" => true,
  ],

  /**
   * Policy to class to use when managing users
   */
  "policy" => Inoby\Laravel\TokenAuth\Policies\UserPolicy::class,
];
