<?php 

namespace Inoby\Laravel\TokenAuth\Services;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Inoby\Laravel\TokenAuth\Mail\PasswordResetCodeMail;
use Inoby\Laravel\TokenAuth\Models\PasswordResetCode;

class PasswordCodeResetService {

  static $PASSWORD_CODE_NOT_EXISTS = "Password code does not exist";
  static $PASSWORD_CODE_EXPIRED = "Password code expired";
  static $PASSWORD_CODE_CHECK_SUCCESS = "Password code check success";

  private $codeExpirationMinutes = 15;

  public function checkCode(string $code, string $email)
  {
    $password_code = PasswordResetCode::firstWhere([
      ["code", "=", $code],
      ["email", "=", $email]
    ]);

    if (!$password_code) {
      return self::$PASSWORD_CODE_NOT_EXISTS;
    }

    if ($password_code->created_at > now()->addMinutes($this->codeExpirationMinutes)) {
      $password_code->delete();
      return self::$PASSWORD_CODE_EXPIRED;
    }

    return self::$PASSWORD_CODE_CHECK_SUCCESS;
  }

  public function sendCode(string $email)
  {    
    // delete old reset codes
    PasswordResetCode::where("email", $email)->delete();

    $code = PasswordResetCode::create([
      "email" => $email,
      "code" => mt_rand(100000, 999999)
    ]);

    $user = config("laravel-token-auth.model")::firstWhere("email", $email);

    return Mail::to($user)->send(new PasswordResetCodeMail($code, $user));
  }

}