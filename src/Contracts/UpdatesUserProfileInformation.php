<?php 

namespace Inoby\Laravel\TokenAuth\Contracts;

interface UpdatesUserProfileInformation {
  public function update($user, $input);
}