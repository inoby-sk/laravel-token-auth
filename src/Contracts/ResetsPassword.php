<?php 

namespace Inoby\Laravel\TokenAuth\Contracts;

/**
 * @method void reset(\Illuminate\Foundation\Auth\User $user, array $input)
 */
interface ResetsPassword {
  // public function reset($user, array $input);
}