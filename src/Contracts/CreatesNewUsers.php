<?php 

namespace Inoby\Laravel\TokenAuth\Contracts;

interface CreatesNewUsers {
  public function create(array $input);
}