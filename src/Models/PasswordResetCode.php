<?php 

namespace Inoby\Laravel\TokenAuth\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordResetCode extends Model {
    const UPDATED_AT = null;

    protected $fillable = [
        'email',
        'code',
        'created_at',
    ];
}