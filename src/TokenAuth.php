<?php

namespace Inoby\Laravel\TokenAuth;

use Inoby\Laravel\TokenAuth\Contracts\CreatesNewUsers;
use Inoby\Laravel\TokenAuth\Contracts\ResetsPassword;
use Inoby\Laravel\TokenAuth\Contracts\UpdatesUserProfileInformation;

class TokenAuth
{
  /**
   * Get model associated with auth
   */
  public static function model()
  {
    return config("laravel-token-auth.model");
  }

  /**
   * Get defined roles
   */
  public static function roles()
  {
    return config("laravel-token-auth.roles");
  }

  /**
   * Check if register is public
   */
  public static function publiclyRegisterable()
  {
    return config("laravel-token-auth.public_register", true);
  }

  /**
   * Determine if the given feature is enabled
   */
  public static function featureEnabled(string $feature)
  {
    return config("laravel-token-auth.features.$feature", false);
  }

  /**
   * Check reset password using code is enabled
   */
  public static function resetPasswordUsingCode()
  {
    return config("laravel-token-auth.reset_password_using", "link") === "code";
  }

  /**
   * Register a class / callback that should be used to create new users.
   *
   * @param  string  $callback
   * @return void
   */
  public static function createUsersUsing(string $callback)
  {
    app()->singleton(CreatesNewUsers::class, $callback);
  }

  /**
   * Register a class / callback that should be used to update user profile information.
   *
   * @param  string  $callback
   * @return void
   */
  public static function updateUserProfileInformationUsing(string $callback)
  {
    app()->singleton(UpdatesUserProfileInformation::class, $callback);
  }

  /**
   * Register a class / callback that should be used to reset users password
   *
   * @param  string  $callback
   * @return void
   */
  public static function resetUserPasswordUsing(string $callback)
  {
    app()->singleton(ResetsPassword::class, $callback);
  }
}
