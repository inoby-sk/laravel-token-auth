<?php 

namespace Inoby\Laravel\TokenAuth\Traits;

trait HasPermissions
{
  /**
   * Determine if the user has permission to the given action
   *
   * @param string $action
   * @return boolean
   */
  public function hasPermissionTo(string $action)
  {
    if (!$this->role) {
      return false;
    }

    [$scope, $permission] = explode(".", $action, 2);

    $permissions = config("laravel-token-auth.permissions.$this->role.$scope", []);

    return $permissions === "*" || in_array($permission, $permissions);
  }
}
