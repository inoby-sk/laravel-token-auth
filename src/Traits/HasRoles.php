<?php 

namespace Inoby\Laravel\TokenAuth\Traits;

trait HasRoles
{
  /**
   * Determine if the user has any of the given roles
   *
   * @param array $roles
   * @return boolean
   */
  public function hasAnyRole(array $roles)
  {
    return in_array($this->role, $roles);
  }


  /**
   * Determine if the user has the given role
   *
   * @param string $roles
   * @return boolean
   */
  public function hasRole(string $role)
  {
    return $this->role === $role;
  }
}
