<?php

namespace Inoby\Laravel\TokenAuth;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class TokenAuthServiceProvider extends ServiceProvider
{
    /**
     * Register the application's policies.
     *
     * @return void
     */
    public function registerPolicies()
    {
        Gate::policy(config("laravel-token-auth.model"), config("laravel-token-auth.policy"));
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(dirname(__DIR__) . "/config/config.php", "laravel-token-auth");
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(dirname(__DIR__) . "/database/migrations");
        $this->loadViewsFrom(dirname(__DIR__) . "/views", "laravel-token-auth");
        $this->loadTranslationsFrom(dirname(__DIR__) . "/lang", "laravel-token-auth");

        $this->configurePublishes();
        $this->registerPolicies();
        $this->configureRoutes();
    }

    protected function configurePublishes()
    {
        $this->publishes(
            [
                dirname(__DIR__) . "/config/config.php" => config_path("laravel-token-auth.php"),
            ],
            "laravel-token-auth-config"
        );

        $this->publishes(
            [
                dirname(__DIR__) . "/lang" => lang_path("vendor/laravel-token-auth"),
            ],
            "laravel-token-auth-lang"
        );

        $this->publishes(
            [
                dirname(__DIR__) . "/stubs/TokenAuthServiceProvider.php" => app_path("Providers/TokenAuthServiceProvider.php"),
                dirname(__DIR__) . "/stubs/Actions/CreateNewUser.php" => app_path("Actions/TokenAuth/CreateNewUser.php"),
                dirname(__DIR__) . "/stubs/Actions/UpdateProfileInformation.php" => app_path("Actions/TokenAuth/UpdateProfileInformation.php"),
                dirname(__DIR__) . "/stubs/Actions/ResetPassword.php" => app_path("Actions/TokenAuth/ResetPassword.php"),
            ],
            "laravel-token-auth-support"
        );
    }

    protected function configureRoutes()
    {
        Route::prefix(config("laravel-token-auth.prefix", "api"))
            ->name("inoby_auth.")
            ->middleware(config("laravel-token-auth.middleware", "api"))
            ->group(function () {
                $this->loadRoutesFrom(dirname(__DIR__) . "/routes/routes.php");
            });
    }
}
