<?php

namespace Inoby\Laravel\TokenAuth\Http\Controllers;

use Inoby\Laravel\TokenAuth\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request)
    {
        $user = Config::get('laravel-token-auth.model')::where("email", $request->validated()["email"])->first();

         if (!$user) {
            throw ValidationException::withMessages([
                "email" => ["User with this email does not exist"],
            ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                "password" => ["The provided credentials are incorrect."],
            ]);
        }

        return response()->json([
            "token" => $user->createToken($request->device_name)->plainTextToken,
            "user" => $user,
        ]);
    }
}