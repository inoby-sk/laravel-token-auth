<?php

namespace Inoby\Laravel\TokenAuth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ChangePasswordController extends Controller
{
  public function changePassword(Request $request)
  {
    $request->validate([
      "old_password" => "required",
      "password" => "required|min:8|confirmed",
    ]);

    $user = $request->user();

    if (!Hash::check($request->old_password, $user->password)) {
      throw ValidationException::withMessages([
        "old_password" => __("Your old password do not match"),
      ])->status(422);
    }

    $user
      ->forceFill([
        "password" => Hash::make($request->password),
      ])
      ->save();

    return response()->json([
      "message" => __("Your password has been changed"),
    ]);
  }
}
