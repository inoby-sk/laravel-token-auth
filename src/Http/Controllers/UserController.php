<?php

namespace Inoby\Laravel\TokenAuth\Http\Controllers;

use Inoby\Laravel\TokenAuth\Contracts\UpdatesUserProfileInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Inoby\Laravel\TokenAuth\TokenAuth;

class UserController extends Controller
{
  public function me(Request $request)
  {
    return response()->json([
      "user" => $request->user(),
    ]);
  }

  public function index(Request $request)
  {
    $users = TokenAuth::model()::all();

    return response()->json([
      "data" => $users,
    ]);
  }

  public function show(Request $request, int $id)
  {
    $user = TokenAuth::model()::findOrFail($id);

    return response()->json([
      "data" => $user,
    ]);
  }

  public function update(
    Request $request,
    UpdatesUserProfileInformation $updater,
    int $id
  ) {
    $user = TokenAuth::model()::findOrFail($id);

    $this->authorize("update", $user);

    if ($updater->update($user, $request->all())) {
      return response([
        "data" => $user->fresh(),
      ]);
    }

    return response(["message" => __("User not updated")], 400);
  }

  public function delete(Request $request, int $id)
  {
    $user = TokenAuth::model()::findOrFail($id);

    $user->delete();

    return response()->json([
      "message" => __("User deleted"),
    ]);
  }
}
