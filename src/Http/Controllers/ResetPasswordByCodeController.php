<?php

namespace Inoby\Laravel\TokenAuth\Http\Controllers;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Inoby\Laravel\TokenAuth\Contracts\ResetsPassword;
use Inoby\Laravel\TokenAuth\Models\PasswordResetCode;
use Inoby\Laravel\TokenAuth\Services\PasswordCodeResetService;
use Inoby\Laravel\TokenAuth\TokenAuth;

class ResetPasswordByCodeController extends Controller
{

    public function __construct(private PasswordCodeResetService $password) {
        
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $status = app('auth.password.broker')->reset(
            $request->only("email", 'password', 'password_confirmation', 'token'),
            function ($user) use ($request) {
                app(ResetsPassword::class)->reset($user, $request->all());

                $user->save();

                event(new PasswordReset($user));
            }
        );

        if ($status === Password::PASSWORD_RESET) {
            return response()->json([
                "message" => __($status),
            ]);
        }

        throw ValidationException::withMessages([
            "email" => __($status)
        ]);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkResetCode(Request $request)
    {
        $request->validate([
            "email" => "required|email|exists:users",
            "code" => "required"
        ]);

        $code = $this->password->checkCode($request->code, $request->email);

        if ($code !== PasswordCodeResetService::$PASSWORD_CODE_CHECK_SUCCESS) {
            throw ValidationException::withMessages(["message" => __($code)]);
        }

        
        $user = TokenAuth::model()::query()->where("email", $request->email)->first();
        
        return response()->json([
            "message" => __("Reset code valid"),
            "token" => app('auth.password.broker')->createToken($user)
        ]);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetCode(Request $request)
    {
        $request->validate([
            "email" => "required|email|exists:users"
        ]);

        if($this->password->sendCode($request->email)) {
            return response()->json(["message" => __("Code sent successfully")]);
        } else {
            throw ValidationException::withMessages(["message" => __("Problem while sending email. Try again.")]);
        }
        
    }
}
