<?php

namespace Inoby\Laravel\TokenAuth\Http\Controllers;

use Illuminate\Http\Request;
use Inoby\Laravel\TokenAuth\Contracts\CreatesNewUsers;
use Inoby\Laravel\TokenAuth\TokenAuth;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{

    public function __invoke(Request $request, CreatesNewUsers $creator)
    {
        // determine if the current user can create new 
        // users / is public registration enabled
        $this->authorize("create", TokenAuth::model());

        if ($user = $creator->create($request->all())) {
            return response()->json(
                [
                    "success" => true,
                ],
                Response::HTTP_CREATED
            );
        }

        return response()->json(
            [
                "success" => false,
            ],
            Response::HTTP_NOT_FOUND
        );
    }
}
