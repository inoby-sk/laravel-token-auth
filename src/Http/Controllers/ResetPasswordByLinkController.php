<?php

namespace Inoby\Laravel\TokenAuth\Http\Controllers;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Inoby\Laravel\TokenAuth\Contracts\ResetsPassword;
use Inoby\Laravel\TokenAuth\Http\Requests\ResetPasswordEmailRequest;

class ResetPasswordByLinkController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $status = Password::reset(
            $request->only("email", 'password', 'password_confirmation', 'token'),
            function ($user) use ($request) {
                app(ResetsPassword::class)->reset($user, $request->all());
            }
        );

        return response()->json(
            [
                "message" => __($status),
            ], 
            $status == Password::PASSWORD_RESET ? 200 : 400
        );
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetLink(ResetPasswordEmailRequest $request)
    {
        $data = $request->validated();

        if ($this->resetPassword($data)) {
            return response()->json(["success" => true]);
        }

        return response()->json(["success" => false], 404);
    }

    protected function resetPassword(array $credentials)
    {
        $status = Password::sendResetLink($credentials);

        return $status === Password::RESET_LINK_SENT;
    }
}
