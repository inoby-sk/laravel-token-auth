<?php

namespace Inoby\Laravel\TokenAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules =  [
            "name" => ["required", "string", "max:255"],
            "email" => ["required", "email", "unique:users", "email"],
            "password" => ["required", "min:6", "confirmed"],
        ];

        if ($roles = config("laravel-token-auth.roles", null)) {
            $rules["role"] = ["nullable", "in:" . implode(",", $roles)];
        }

        return $rules;
    }
}
